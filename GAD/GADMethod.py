#!/usr/bin/python

import re,os
from collections import OrderedDict


class GADMethod:

    def __init__(self, elements=[]):
        self.__elements={}
        for e in elements:
            self.__elements[e]=1


    def processRawGADFile(self,annoFile,noMesh,tmp,geneGAD):

        gad_hash = OrderedDict()

        fh = open(annoFile)
        wh1 = open(geneGAD,"w")
        wh2 = open(noMesh,"w")
        wh3 = open(tmp,"w")

        for lines in fh:
            lines = lines.strip()
            if not re.search("^Genetic|^Tab|^ID",lines):
                strs = re.split("\t",lines)
                
                try:
                    gene_id = strs[8].strip().upper()
                    #pheno1 = strs[2].strip()
                    #pheno2 = strs[5].strip()
                    pheno = strs[5].strip()
                    #pheno = pheno1+pheno2

                    if pheno !='':

                        if gad_hash.has_key(gene_id):

                            tmp = re.split("\|",gad_hash[gene_id])
                            tmp.append(pheno)
                            gad_hash[gene_id] = "|".join(list(set(tmp)))
                            tmp=[]

                        else:
                            if gene_id !='':
                                gad_hash[gene_id] = pheno
                    else:
                        print >>wh2, "ELSE: ",strs

                except IndexError:
                    print >>wh3,"Exception: ",strs
                    pass

        for keys in gad_hash:
            print >>wh1,keys+"\t"+gad_hash[keys]

        wh1.close()
        wh2.close()
        wh3.close()

        

