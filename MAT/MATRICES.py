#!/usr/bin/python

import re
import sys 


class MATRICES:

    def __init__(self, elements=[]):
        self.__elements={}
        for e in elements:
            self.__elements[e]=1


    def getSparseMat(self, inp_file, row_file, col_file, sparse_file,anno_type=[]):

        gene_name = {}
        ont_list = {}
        ind_ont = {}

        gene_count = 1
        fh = open(inp_file)
        lines = fh.readline()

        while lines:
            lines = lines.strip()
            strs = re.split("\t",lines)
            strs = [x.strip() for x in strs]
            gene_id = strs[0]
            gene_name[gene_count] = gene_id
            #gene_name.append(gene_id)
            
            if len(strs) >1:

                if re.search("\|",strs[1].strip()):
                    ont_strs =  re.split("\|",strs[1].strip())

                else:
                    ont_strs =  re.split("\,|\s",strs[1].strip())
                ont_strs = [x for x in ont_strs if x!='']
                for e in ont_strs:
                    e = e.strip()
                    ont_list[e] = 1

            if len(strs)==3:
                ont_parents = re.split("\,",strs[2])
                for e in ont_parents:
                    e = e.strip()
                    ont_list[e] = 1
            gene_count = gene_count+ 1
            lines = fh.readline()

        gene_list = gene_name.values()
        comb_ont = ont_list.keys()
        col_count = 1
        for ele in comb_ont:
            #print ele
            ind_ont[ele] = col_count#comb_ont.index(ele)+1
            col_count = col_count+1

        self.printRow(row_file, gene_name)
        self.printCol(col_file, ind_ont) #mat_col_names)
        self.printSparse(inp_file,sparse_file,ind_ont)

        fh.close()
        #return mat_row_names, mat_col_names, ind_ont


    def printRow(self, row_file, gene_name):
           
        print 'inside row'
        wh = open(row_file,"w")

        keylist = gene_name.keys()
        keylist.sort()
        for key in keylist:
            print >>wh,"%s\t%s" % (key,gene_name[key])

        wh.close()


    def printCol(self, col_file, ind_ont):
        
        wh = open(col_file,"w")

        for key,value in sorted(ind_ont.iteritems(),key=lambda(k,v):(v,k)):
            print >>wh,"%s\t%s" % (key,value)

        wh.close()

            

    def printSparse(self, inp_file,out_sparse, ind_ont):

        fh = open(inp_file)
        wh = open(out_sparse,"w")

        lines = fh.readline()
        row_ind_count = 0

        while lines:

            lines = lines.strip()
            strs = re.split("\t",lines)
            ont_hash = {}
            gene_id = strs[0].strip()
            row_ind_count = row_ind_count+1
            
            if len(strs) >1:
                if re.search("\|",strs[1].strip()):
                    ont_gene = re.split("\|",strs[1].strip())
                else:
                    ont_gene = re.split("\,|\s",strs[1].strip())

                ont_gene = [x for x in ont_gene if x!='']
                for e in ont_gene:
                    e = e.strip()
                    ont_hash[e] = 1

            if len(strs) ==3:
                ont_parents = re.split("\,",strs[2].strip())
                for e in ont_parents:
                    e = e.strip()
                    ont_hash[e] = 1

            for e in ont_hash:
                e = e.strip()
                ind = ind_ont[e]
                print >>wh, row_ind_count,"\t",ind
            lines = fh.readline()

        fh.close()
        wh.close()


    def getPubSparseMat(self,stemmed_file,row_file,col_file,sparse_file):

        fh = open(stemmed_file)
        cfh = open(stemmed_file)

        wrh = open(row_file,"w")
        wch = open(col_file,"w")
        wsh = open(sparse_file,"w")

        words_index_hash = {}
        gene_word_freq_hash = {}

        index_count = 1

        for lines in fh:
            word_freq_hash = {}
            lines = lines.strip()
            try:
                strs = re.split("\t",lines)
                gene_id = strs[0].strip()
                gene_id_count = 1
                abstr_str = re.split("\s",strs[1].strip())

            except IndexError:
                abstr_str = []
            count = []

            for ele in abstr_str:
                ele = ele.strip()
                if word_freq_hash.has_key(ele):
                    count = word_freq_hash[ele]
                    count = count+1
                    word_freq_hash[ele] = count
                else:
                    word_freq_hash[ele] = 1
                    words_index_hash[ele] = 1
                    count = []
            gene_word_freq_hash[gene_id] = word_freq_hash

        master_word_list = words_index_hash.keys()

        for ele in master_word_list:
            ele = ele.strip()
            words_index_hash[ele] = index_count
            index_count = index_count + 1

        for keys in sorted(words_index_hash, key=words_index_hash.get,reverse=False):
            print >>wch,keys+"\t"+str(words_index_hash[keys])

        gene_row_ind = 1
        for lines in cfh:
            lines = lines.strip()
            strs = re.split("\t",lines)
            try:
                gene_id = strs[0].strip()
                print >>wrh,gene_id
                abstr_str = re.split("\s",strs[1].strip())
                for ele in abstr_str:
                    ele = ele.strip()
                    col_ind = words_index_hash[ele]
                    word_freq_hash = gene_word_freq_hash[gene_id]
                    freq_count = word_freq_hash[ele]
                    print >>wsh,str(gene_row_ind)+"\t"+str(col_ind)+"\t"+str(freq_count)
                gene_row_ind = gene_row_ind+1
            except IndexError:
                gene_row_ind = gene_row_ind+1

        wrh.close()
        wch.close()
        wsh.close()

