#!/usr/bin/python

import re,os
from collections import OrderedDict


class HUGEMethod:

    def __init__(self, elements=[]):
        self.__elements={}
        for e in elements:
            self.__elements[e]=1


    def processRawHUGEFile(self,annoFile,geneHUGE):

        huge_hash = OrderedDict()

        fh = open(annoFile)
        wh = open(geneHUGE,"w")

        for lines in fh:
            lines = lines.strip()
            if not re.search("^MeSH|^Total|^Data",lines):
                strs = re.split("\t",lines)
                #print strs
                
                try:
                    mesh_term = re.split("\(|\)",strs[0])[0]
                    gene_id_list = strs[1:len(strs)]
                    for ele in gene_id_list:
                        strs1 = re.split("\(|\)",ele)
                        gene_id = strs1[0].strip().upper()
                        
                        if huge_hash.has_key(gene_id):
                            tmp = re.split("\|",huge_hash[gene_id])
                            tmp.append(mesh_term)
                            huge_hash[gene_id] = "|".join(list(set(tmp)))
                            tmp = []
                        else:
                            huge_hash[gene_id] = mesh_term

                except IndexError:
                    print "Exception: ",strs
                    pass

        for keys in huge_hash:
            print >>wh,keys+"\t"+huge_hash[keys]

        wh.close()

        

