#!/usr/bin/python

import re

class HGNC:

    def __init__(self,elements=[]):
        self.__elements={}
        for e in elements:
            self.__elements[e]=1

    def display(self):
        print "Inside HGNC Class"


    def parseHGNC(self, inp_file, out_file):


        hgnc_ensg = {}
        ensg_hgnc = {}

        fh = open(inp_file)
        for lines in fh:
            lines = lines.strip()
            if not re.search("^Gene|HGNC",lines):
                strs = re.split("\t",lines)
                try:
                    hgnc_id = strs[1].strip()
                    ensg_id = strs[0].strip()
                    if hgnc_ensg.has_key(hgnc_id):
                        tmp = hgnc_ensg[hgnc_id]
                        tmp.append(ensg_id)
                        hgnc_ensg[hgnc_id] = tmp
                    else:
                        hgnc_ensg[hgnc_id] = [ensg_id]
                except:
                    pass

        wh = open(out_file,"w")
        for keys in hgnc_ensg:
            ensg_ids = hgnc_ensg[keys]
            print >>self.wh,keys+"\t"+ensg_ids[0]
            ensg_id = ensg_ids[0]
            hgnc_id = keys	
            if ensg_hgnc.has_key(ensg_id):
                tmp = ensg_hgnc[ensg_id]
                tmp.append(hgnc_id)
                ensg_hgnc[ensg_id] = tmp
            else:
                ensg_hgnc[ensg_id] = [hgnc_id]

        fh.close()
        wh.close()

        return ensg_hgnc


    def getEnsgHgncDict(self,inp_file):
        fh = open(inp_file)
        hgnc_ensg = {}
        for lines in fh:
            lines = lines.strip()
            strs = re.split("\t",lines)
            
            try:
                hgnc_id = strs[0].strip()
            except:
                hgnc_id = 'NA'
            try:
                ensg_id = strs[1].strip()
            except:
                ensg_id = 'NA'
            
            if hgnc_ensg.has_key(hgnc_id):
                tmp = hgnc_ensg[hgnc_id]
                tmp.append(ensg_id)
                hgnc_ensg[hgnc_id] = tmp
            else:
                hgnc_ensg[hgnc_id] = [ensg_id]
                
        return hgnc_ensg

    def processRawHGNCMapFile(self,inp_hugo_map_file,out_hgnc_ensg_file,pbrit_hgnc_list):
        ''' Function to process Raw HGNC file to extract and map Approved 
            HGNC and Ensembl'''
        
        fh = open(inp_hugo_map_file)

        hgnc_ensg = {}
        ensg_hgnc = {}

        for lines in fh:
            lines = lines.strip()
            strs = re.split('\t',lines)
            approved_symb = re.split('\,',strs[1].strip())

            try:
                prev_symb = re.split('\,',strs[4].strip())
            except:
                prev_symb = []
        
            try:
                alias_symb = re.split('\,',strs[5].strip())
            except:
                alias_symb = []

            #symbol_strs = approved_symb
            #symbol_strs = approved_symb+alias_symb
            symbol_strs = approved_symb+prev_symb+alias_symb
            symbol_strs = [x.strip().upper() for x in symbol_strs if x]
            symbol_strs = list(set(symbol_strs))

            try:
                ensg_list = re.split('\,',strs[9].strip())
            except:
                ensg_list = ['NA']

            if ensg_list[0] !='' and ensg_list[0] !='NA':

                for symbol_id in symbol_strs:
                    symbol_id = symbol_id.upper()

                    if hgnc_ensg.has_key(symbol_id):
                        tmp = hgnc_ensg[symbol_id]
                        tmp.append(ensg_list[0])
                        hgnc_ensg[symbol_id] = list(set(tmp))
                    else:
                        hgnc_ensg[symbol_id] = ensg_list
                

                for ensg_id in ensg_list:
                    if ensg_hgnc.has_key(ensg_id):
                        tmp = ensg_hgnc[ensg_id]
                        tmp += symbol_strs
                        tmp = [x.upper() for x in tmp]
                        ensg_hgnc[ensg_id] = list(set(tmp))
                    else:
                        ensg_hgnc[ensg_id] = symbol_strs


        fh.close()
        
        #Output HGNC-ENSG and ENSG-HGNC Map file
        wh = open(out_hgnc_ensg_file,'w')
        
        for hgnc_id,ensg_id in hgnc_ensg.items():
            if hgnc_id in pbrit_hgnc_list:
                print >>wh,hgnc_id+'\t'+ensg_id[0]

        wh.close()

