#!/bin/bash


#PBS -N TFIDF_PPI
#PBS -d /home/shared_data_medgen_aorta/pbrit_data/V3/HUGO
#PBS -o /home/shared_data_medgen_aorta/pbrit_data/V3/HUGO/QOUT/MAP_qsub_o.txt
#PBS -e /home/shared_data_medgen_aorta/pbrit_data/V3/HUGO/QOUT/MAP_qsub_e.txt
#PBS -l nodes=1:ppn=12,mem=30gb


/opt/software/R/3.2.1/bin/Rscript mapHgnc2Ensg.R -i /home/shared_data_medgen_aorta/pbrit_data/V3/HUGO/HGNC_ENSG.txt -o /home/shared_data_medgen_aorta/pbrit_data/V3/


